# Security Policy Project for DevSecOps-Demo

This project is managing security policies for the group [DevSecOps Demo](https://gitlab.com/universalamateur1/devsecops-demo).

The Security Policies Project is a repository used to store policies. All security policies are stored as a YAML file named `.gitlab/security-policies/policy.yml`.

You can read more about the format and policies schema in the [documentation](https://gitlab.com/help/user/application_security/policies/scan-execution-policies#scan-execution-policy-schema).

## Default branch protection settings

This project is preconfigured with the default branch set as a protected branch, and only maintainers/owners of
[DevSecOps-Demo](https://gitlab.com/groups/universalamateur1/devsecops-demo) have permission to merge into that branch. This overrides any default branch protection both at the
[group level](https://gitlab.com/help/user/group/manage#change-the-default-branch-protection-of-a-group) and at the
[instance level](https://gitlab.com/help/user/project/repository/branches/default#instance-level-default-branch-protection).
